interface RPCRequestBody{
  method: string;
  arguments?: { [key: string]: string | object | boolean | number };
}

interface RPCResponseBody{
  arguments: { [key: string]: string | object | boolean | number };
  result: string;
}

interface TorrentOptions{
  filename?: string;
  metainfo?: string;
  'download-dir'?: string;
  paused?: boolean;
}

export default class Transmission{
  private host: string
  private port: number
  private sessionId: string
  constructor(host = 'localhost', port = 9091){
    this.host = host
    this.port = port
    this.sessionId = ''
  }
  private request(endpoint: string, method = 'GET', options?: RequestInit): Promise<{[key: string]: string | object}>{
    const opts: RequestInit = {
      method,
      headers: {
        'X-Transmission-Session-Id': this.sessionId,
      },
      body: ''
    }
    if(options){
      if(options.headers){
        opts.headers = {...opts.headers, ...options.headers}
      }
      if(options.body){
        opts.body = options.body
      }
    }
    return fetch(`http://${this.host}:${this.port}${endpoint}`, opts)
      .then((resp) => {
        if (resp.status === 409) {
          const id = resp.headers.get('X-Transmission-Session-Id')
          if (id) {
            this.sessionId = id
            return this.request(endpoint, method, options)
          }
        }
        if(resp.headers.get('content-type')?.indexOf('application/json') === 0){
          return resp.json()
        } else {
          return resp.text()
        }
      })
      .catch((resp: Response) => {
        console.log(resp)
      })
  }
  private rpc(method: string, args?: { [key: string]: string | object | boolean | number }){
    const body: RPCRequestBody = {
      method
    }
    if (args) {
      body.arguments = args
    }
    const options: RequestInit = {
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify(body)
    }
    return this.request('/transmission/rpc', 'POST', options)
    .then((resp: any) => {
      if (resp.result === 'success') {
        return resp.arguments
      }
      return resp
    })
  }
  public getSession(){
    return this.rpc('session-get')
  }
  public getAllTorrents(fields = ['id', 'name', 'error', 'errorString', 'eta', 'isFinished', 'isStalled', 'leftUntilDone', 'metadataPercentComplete', 'peersConnected', 'peersGettingFromUs', 'peersSendingToUs', 'percentDone', 'queuePosition', 'rateDownload', 'rateUpload', 'recheckProgress', 'sizeWhenDone', 'status', 'downloadDir', 'uploadedEver', 'uploadRatio']){
    return this.rpc('torrent-get', {
      fields
    })
  }
  public getTorrents(ids: Array<number> = [], fields = ['id', 'error']){
    return this.rpc('torrent-get', {
      fields,
      ids
    })
  }
  public getSessionStats(){
    return this.rpc('session-stats')
  }
  public startTorrent(id: number){
    return this.rpc('torrent-start', {
      ids: id
    })
  }
  public stopTorrent(id: number){
    return this.rpc('torrent-stop', {
      ids: id
    })
  }
  public removeTorrents(ids: Array<number>, deleteData = false){
    return this.rpc('torrent-remove', {
      ids,
      'delete-local-data': deleteData
    })
  }
  public addTorrent(options: TorrentOptions){
    return this.rpc('torrent-add', options )
  }
}