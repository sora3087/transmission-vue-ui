import Transmission from "@/lib/Transmission"

const transmissionService = new Transmission(window.location.hostname);

export default transmissionService