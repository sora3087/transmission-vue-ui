import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import './styles/mdl2.scss'
import './styles/tag.scss'
import './styles/v-context.scss'
import './styles/vm--modal.scss'
import './styles/button.scss'
import '../node_modules/bulma/bulma.sass'
import VModal from 'vue-js-modal'

Vue.config.productionTip = false

Vue.use(VModal)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
