import Vue from 'vue'
import VueRouter from 'vue-router'
import moduleProvider from '@/providers/modulesProvider'
// import './register-hooks'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: moduleProvider.getRoutes()
})
export default router
