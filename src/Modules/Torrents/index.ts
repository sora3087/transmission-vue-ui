import IModule from '../IModule';
import TorrentsView from './Torrents.vue'
import ModuleManager from '../ModuleManager';
import Pieces from './Modules/Pieces';

const detailTabs = new ModuleManager([ new Pieces() ])

export {detailTabs}

export default class Torrents implements IModule{
  public getNavItem() {
    return {
      icon: 'all-apps',
      link: '/torrents',
      label: this.constructor.name
    }
  }
  public getRoutes() {
    return [{
      path: '/torrents',
      name: this.constructor.name,
      component: TorrentsView,
      children: detailTabs.getRoutes().map(route => {
        route.path = ':id'+route.path
        return route
      })
    }]
  }
}
