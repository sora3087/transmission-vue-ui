import IModule from '../../../IModule';
import PiecesView from './Pieces.vue'
export default class Pieces implements IModule{
  public getNavItem() {
    return {
      icon: 'oem',
      link: '/pieces',
      label: this.constructor.name
    }
  }
  public getRoutes() {
    return [
      {
        path: '/pieces',
        name: this.constructor.name,
        component: PiecesView
      },
      {
        path: '',
        name: this.constructor.name,
        component: PiecesView
      }
    ]
  }
}