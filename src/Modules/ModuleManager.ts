import IModule from './IModule';
import { RouteConfig } from 'vue-router';

export default class ModuleManager {
  protected _modules: Array<IModule> = []
  constructor(modules: Array<IModule>){
    this._modules = modules
  }
  getNavItems(){
    return this._modules.map(module => module.getNavItem())
  }
  getRoutes(){
    const routes: RouteConfig[] = []
    this._modules.forEach(module => {
      module.getRoutes().forEach(route => {
        routes.push(route)
      })
    })
    return routes
  }
}