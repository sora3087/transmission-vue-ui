import INavItem from './INavItem'
import { RouteConfig } from 'vue-router'

export default interface IModule{
  getNavItem: () => INavItem
  getRoutes: () => RouteConfig[]
}