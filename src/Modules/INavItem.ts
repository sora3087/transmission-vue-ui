export default interface INavItem{
  icon: string
  link: string
  label: string
}