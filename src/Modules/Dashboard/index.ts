import IModule from '../IModule';
import DashboardView from './Dashboard.vue'

export default class Dashboard implements IModule{
  getNavItem() {
    return {
      icon: 'tiles',
      link: '/dashboard',
      label: 'Dashboard'
    }
  }
  getRoutes() {
    return [{
      path: '/dashboard',
      name: 'Dashboard',
      component: DashboardView
    }]
  }
}