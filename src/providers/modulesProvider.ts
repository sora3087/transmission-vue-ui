import ModuleManager from "@/Modules/ModuleManager";
import Dashboard from '@/Modules/Dashboard';
import Torrents from '@/Modules/Torrents';

const moduleProvider = new ModuleManager([
  new Dashboard(),
  new Torrents()
])

export default moduleProvider